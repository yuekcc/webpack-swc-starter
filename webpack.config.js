const path = require('path');

const { VueLoaderPlugin } = require('vue-loader');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  mode: 'development',
  entry: {
    index: './src/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist',
    clean: true,
  },
  resolve: {
    extensions: ['.vue', '.ts', '.tsx', '...'],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.[jt]sx?$/,
        use: '@swc-node/loader',
      },
    ],
  },
  plugins: [new VueLoaderPlugin()],
  optimization: {
    minimize: true,
  },

  devServer: {
    static: 'public',
    compress: true,
  },
};
