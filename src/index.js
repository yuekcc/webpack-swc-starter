import { getMessage } from './lib';
import Vue from 'vue';
import App from './app';

async function main() {
  const msg = await getMessage();
  console.log(msg);

  const Root = Vue.extend(App);
  new Root({
    el: '#app',
  });
}

main().catch(err => console.error(err));
